package com.s.hellositter.presentation.drawerparentmenu.invitefriends

import android.content.Intent
import com.s.hellositter.presentation.base.BaseView

interface InviteFriendsView: BaseView {

    fun send(intent: Intent)

    fun sendMail(intent: Intent)
}