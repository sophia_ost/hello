package com.s.hellositter.data.user

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

import com.s.hellositter.data.base.BaseRepo
import com.s.hellositter.domain.model.User
import com.s.hellositter.domain.user.UserRepo

class UserRepoDatabase @Inject constructor() : BaseRepo(), UserRepo {

    override fun getAll(lifecycleHolder: LifecycleOwner?): Observable<List<User>> = PublishSubject.create<List<User>> {
        lifecycleHolder?.apply {
            database.user().getAll()?.observe(this, Observer<List<User>> { t ->
                t?.apply {
                    it.onNext(t)
                }
            })
        }
    }

    override fun getAllStatic(): List<User>? = database.user().getAllStatic()

    override fun save(user: User) = database.user().save(user)

    override fun deleteAllTable() = database.user().deleteAllTable()

    override fun deleteById(id: String) = database.user().deleteById(id)

}
