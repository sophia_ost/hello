package com.s.hellositter.presentation.bookings

import android.view.WindowManager
import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.withActivity

class BookingsFragment: BaseFragment<BookingsViewModel>(), BookingsView {

    override fun getLayoutResource(): Int = R.layout.fragment_bookings

    override fun getViewModelClass(): Class<BookingsViewModel> = BookingsViewModel::class.java

    override fun viewInit() {
        withActivity<BaseActivity> {
            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
            showBottomNavigation()
            supportActionBar?.show()
        }
    }
}