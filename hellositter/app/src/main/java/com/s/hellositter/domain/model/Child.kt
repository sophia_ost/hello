package com.s.hellositter.domain.model

import android.graphics.drawable.Drawable

data class Child(
    var name: String?,
    var image: Drawable?,
    var dateOfBirth: String?,
    var specialNeeds: String?,
    var allergies: String?,
    var bedtimeRoutine: String?,
    var notes: String?,
    var parentId: Int?
)