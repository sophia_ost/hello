package com.s.hellositter

import android.app.Activity
import android.content.SharedPreferences
import android.graphics.Typeface
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toolbar
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

import com.s.hellositter.presentation.base.BaseView

fun Disposable.toDisposables(disposables: CompositeDisposable) {
    disposables.add(this)
}

//SharedPreference
fun SharedPreferences.putString(key: String, value: String?) = edit().putString(key, value).commit()

fun SharedPreferences.putLong(key: String, value: Long) = edit().putLong(key, value).commit()

//Any
fun Any.toJson(): String = Gson().toJson(this)

inline fun <reified T> String.toObject(): T = Gson().fromJson(this, T::class.java)

fun <T> Observable<T>.applySchedulers(): Observable<T> =
    subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

fun <T> Observable<T>.default(function: (e: Throwable) -> Unit): Observable<T> =
    applySchedulers().propagateErrors(function).retry()

fun <T> Observable<T>.defaultActions(view: BaseView?): Observable<T> =
    doOnSubscribe { view?.showProgress() }
        .doFinally { view?.hideProgress() }
        .doOnTerminate { view?.hideProgress() }
        .doOnError { view?.hideProgress() }
        .doOnComplete { view?.hideProgress() }

fun <T> Observable<T>.propagateErrors(function: (error: Throwable) -> Unit): Observable<T> =
    doOnError {
        function(it)
    }.onExceptionResumeNext(Observable.empty<T>())
        .onErrorResumeNext(Observable.empty<T>())
        .onExceptionResumeNext(Observable.empty<T>())

fun View.showSoftKeyboard() =
    (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
        .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)

fun View.hideSoftKeyboard() =
    (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
        .hideSoftInputFromWindow(this.getWindowToken(), 0)


