package com.s.hellositter.presentation.drawerparentmenu.help

import android.content.Intent
import com.s.hellositter.presentation.base.BaseView

interface HelpView: BaseView {
    fun call(intent: Intent)

}