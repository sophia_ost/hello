package com.s.hellositter.presentation.drawerparentmenu.address.addressnew

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseFragment

class AddressNewFragment: BaseFragment<AddressNewViewModel>(), AddressNewView {

    override fun getLayoutResource(): Int = R.layout.fragment_address_new

    override fun getViewModelClass(): Class<AddressNewViewModel> = AddressNewViewModel::class.java
}