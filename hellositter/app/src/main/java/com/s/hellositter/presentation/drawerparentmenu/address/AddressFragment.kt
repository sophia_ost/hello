package com.s.hellositter.presentation.drawerparentmenu.address

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.navigate
import com.s.hellositter.presentation.withActivity
import kotlinx.android.synthetic.main.activity_base.*

class AddressFragment: BaseFragment<AddressViewModel>(), AddressView {

    override fun getLayoutResource(): Int = R.layout.fragment_drawer_address

    override fun getViewModelClass(): Class<AddressViewModel> = AddressViewModel::class.java

    override fun viewInit() {
        withActivity<BaseActivity> {
            hideBottomNavigation()

        }
    }

    override fun navigateBack() {
        this@AddressFragment.navigate(R.id.action_address_fragment_to_book_tab)
    }
}