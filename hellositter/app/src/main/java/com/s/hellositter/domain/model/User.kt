package com.s.hellositter.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

import com.s.hellositter.TABLE_USERS

@Entity(tableName = TABLE_USERS)
@Parcelize
data class User(
    var name: String? = ""

) : Parcelable {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

}


