package com.s.hellositter.presentation.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton
import com.s.hellositter.App
import com.s.hellositter.data.di.BaseModule
import com.s.hellositter.data.di.RepoBindModule
import com.s.hellositter.presentation.book.list.ChildContentViewModel

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        BaseModule::class,
        UiInjectionsModule::class,
        RepoBindModule::class,
//db
        ViewModelFactoryModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {

    fun inject (model: ChildContentViewModel)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}
