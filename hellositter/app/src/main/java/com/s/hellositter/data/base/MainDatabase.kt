package com.s.hellositter.data.base

import androidx.room.Database
import androidx.room.RoomDatabase
import com.s.hellositter.domain.model.User
import com.s.hellositter.domain.user.UserDao

@Database(entities = [User::class], version = 1, exportSchema = false)
abstract class MainDatabase : RoomDatabase() {
    abstract fun user(): UserDao
}
