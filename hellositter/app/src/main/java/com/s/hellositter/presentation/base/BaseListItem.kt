package com.s.hellositter.presentation.base

interface BaseListItem : IdHolder {
    val layoutRes: Int
}
