package com.s.hellositter.presentation.drawerparentmenu.settings

import com.s.hellositter.presentation.base.BaseView

interface SettingsView: BaseView {

    fun changePassClicked()
}