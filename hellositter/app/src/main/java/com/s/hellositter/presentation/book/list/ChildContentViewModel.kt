package com.s.hellositter.presentation.book.list

import com.s.hellositter.R
import com.s.hellositter.domain.model.Child
import com.s.hellositter.presentation.base.BaseListItem
import com.s.hellositter.presentation.base.ItemCLickListener
import com.s.hellositter.presentation.book.BookView

class ChildContentViewModel(val view: BookView?, val item: Child, val listener: ItemCLickListener): BaseListItem {

    override val layoutRes: Int = R.layout.item_book_child
    override var comparableId: String? =""


}