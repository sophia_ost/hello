package com.s.hellositter

import androidx.room.Room
import android.content.Context
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import io.reactivex.plugins.RxJavaPlugins

import com.s.hellositter.presentation.di.AppComponent
import com.s.hellositter.presentation.di.DaggerAppComponent
import com.s.hellositter.data.base.MainDatabase
import android.R
import com.jakewharton.threetenabp.AndroidThreeTen


class App : DaggerApplication() {

    companion object {
        @JvmStatic
        private lateinit var instance: App

        @JvmStatic
        fun getContext(): Context = instance

        @JvmStatic
        fun get(): App = getContext() as App

        val database by lazy {
            Room.databaseBuilder(getContext(), MainDatabase::class.java, MY_DATABASE)
                .fallbackToDestructiveMigration()
                .build()
        }

    }

    lateinit var appComponent: AppComponent

    init {
        instance = this
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerAppComponent.builder().application(this).build().apply {
            appComponent = this
            appComponent.inject(this@App)
        }

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        registerErrorHandler()
    }

    fun registerErrorHandler() {
        RxJavaPlugins.setErrorHandler {
            if (it is ArrayIndexOutOfBoundsException && it.message?.contains("length=12; index=-1") == true) {

            }
        }
    }





}
