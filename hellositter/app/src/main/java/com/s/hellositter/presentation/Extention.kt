@file:Suppress("NOTHING_TO_INLINE")

package com.s.hellositter.presentation

import android.R
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.databinding.ObservableField
import android.graphics.*
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.*
import androidx.databinding.InverseBindingListener
import androidx.navigation.Navigation
import com.google.android.material.navigation.NavigationView
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.BasePermissionListener
import com.karumi.dexter.listener.single.CompositePermissionListener
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener
import org.jetbrains.anko.bundleOf
import org.threeten.bp.DayOfWeek
import org.threeten.bp.temporal.WeekFields
import java.util.*

fun androidx.fragment.app.Fragment.navigate(@IdRes action: Int, @IdRes fragmentId: Int, closure: Bundle.() -> Unit) {
    try {
        if (view?.id == fragmentId)
            view?.apply {
                Navigation.findNavController(this).navigate(action, bundleOf().apply { closure(this) })
            }
    } catch (e: Exception) {
    }
}

fun Fragment.navigate(@IdRes action: Int) {
    try {
        view?.apply {
            Navigation.findNavController(this).apply {
                navigate(action, bundleOf())
            }
        }
    } catch (e: Exception) {

    }

}

fun androidx.fragment.app.Fragment.navigate(@IdRes action: Int, @IdRes fragmentId: Int, vararg extras: Pair<String, Any?>) {
    try {
        view?.apply {
            Navigation.findNavController(this).apply {
                if (currentDestination?.id == fragmentId)
                    navigate(action, bundleOf(*extras))
            }
        }
    } catch (e: Exception) {
    }
}

fun Activity.permissionCheck(permission: String, permissionName: String? = null, closure: Boolean.() -> Unit) {
    hideSoftKeyboard()
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
        val list = permission.split(".")
        var permissionTitle = permission
        if (list.isNotEmpty())
            permissionTitle = list.last().toLowerCase().replace("_", " ")
        Dexter.withActivity(this)
            .withPermission(permission)
            .withListener(
                CompositePermissionListener(DialogOnDeniedPermissionListener.Builder
                    .withContext(this)
                    //.withTitle(R.string.permission_title)
                    //.withMessage(this.getString(R.string.permission_message_pattern, permissionName
                    //        ?: permissionTitle))
                    .withButtonText(android.R.string.ok)
                    .build(),
                    object : BasePermissionListener() {
                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                            closure(true)
                        }

                        override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                            closure(false)
                        }
                    })
            )
            .withErrorListener { throw IllegalStateException(it.toString()) }
            .check()
    } else
        closure(true)
}

fun Context.getScreenAspectRatio(higherThanOne: Boolean = true): Float {
    val point = getScreenPointDimens()
    return if (higherThanOne) {
        if (point.y > point.x) point.y.toFloat() / point.x.toFloat() else point.x.toFloat() / point.y.toFloat()
    } else {
        if (point.y < point.x) point.y.toFloat() / point.x.toFloat() else point.x.toFloat() / point.y.toFloat()
    }
}

fun Context.getScreenPointDimens(): Point {
    val point = Point()
    (getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
        .getSize(point)
    return point
}

fun Activity.getScreenRect(): Rect {
    val displayRectangle = Rect()
    window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
    return displayRectangle
}

fun androidx.fragment.app.Fragment.getScreenRect(): Rect {
    val displayRectangle = Rect()
    activity?.window?.decorView?.getWindowVisibleDisplayFrame(displayRectangle)
    return displayRectangle
}

fun Context.dpToPixel(@Dimension(unit = Dimension.DP) dp: Float): Int =
    (dp * resources.displayMetrics.density).toInt()

fun Context.dp(@Dimension(unit = Dimension.DP) dp: Float): Float =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics())

inline fun <reified T : Activity?> androidx.fragment.app.Fragment.withActivity(block: T.() -> Unit) =
    activity?.let { block(activity as T) }

inline fun <reified T : androidx.fragment.app.Fragment> T.withContext(block: T.(context: Context) -> Unit) {
    activity?.apply {
        block(this)
    }
}

fun Activity.viewLink(link: String = "http://www.google.com") {
    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(link)));
}

fun ViewGroup.inflateView(@LayoutRes layout: Int): View =
    LayoutInflater.from(context).inflate(layout, this, false)


fun androidx.fragment.app.Fragment.getImageFromFilesystem(requestCode: Int, multipleFiles: Boolean = false, videoFiles: Boolean = false) {
    withActivity<Activity> {
        permissionCheck(android.Manifest.permission.READ_EXTERNAL_STORAGE) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, multipleFiles)
            intent.type = "image/*, video/*"
            this@getImageFromFilesystem.startActivityForResult(intent, requestCode)
        }
    }
}

//SharedPreference

@SuppressLint("ApplySharedPref")
fun SharedPreferences.putString(key: String, value: String) = edit().putString(key, value).commit()

//Keyboard

fun Activity.showSoftKeyboard() =
    (getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
        .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)

fun View.showSoftKeyboard() =
    (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
        .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)

fun View.hideSoftKeyboard() =
    (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
        .hideSoftInputFromWindow(this.getWindowToken(), 0)


fun Activity.hideSoftKeyboard() {
    if (currentFocus != null)
        if (currentFocus.windowToken != null)
            (getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
                .hideSoftInputFromWindow(currentFocus.windowToken, 0)
}

fun Context.isConnected(): Boolean {
    val activeNetwork = (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
    return activeNetwork != null && activeNetwork.isConnectedOrConnecting
}

fun androidx.fragment.app.Fragment.getImageFromCamera(requestCode: Int) {
    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    this@getImageFromCamera.startActivityForResult(intent, requestCode)
}

fun <T : androidx.fragment.app.Fragment> T.withExtras(extras: Bundle.() -> Unit): T =
    this.apply {
        bundleOf().apply {
            extras(this)
            arguments = this
        }
    }

inline fun <T : Any?> Bundle?.value(key: String): T {
    @Suppress("UNCHECKED_CAST")
    return this?.get(key) as T
}

fun Activity.getImageFromFilesystem(requestCode: Int, multipleFiles: Boolean = false) {
    permissionCheck(android.Manifest.permission.READ_EXTERNAL_STORAGE) {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, multipleFiles)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*, video/*"
        startActivityForResult(intent, requestCode)
    }
}

fun Activity.getImageFromCamera(requestCode: Int) {
    permissionCheck(android.Manifest.permission.CAMERA) {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, requestCode)
    }
}


fun ImageView.loadBitmapFromFile(file: Uri) {
    setImageBitmap(MediaStore.Images.Media.getBitmap(this.context.contentResolver, file))
}

inline operator fun <reified T : Any?> Bundle.invoke(key: String): T? = get(key) as? T?

inline fun <reified T> Context.systemService(name: String) = getSystemService(name) as T

fun ClipData.items() = iterator().asSequence().toList()

operator fun TabLayout.iterator() = object : Iterator<TabLayout.Tab> {
    private var index = 0
    override fun hasNext() = index < tabCount
    override fun next() = getTabAt(index++) ?: throw IndexOutOfBoundsException()
}

val TabLayout.tabs
    get() = iterator().asSequence().toList()

operator fun ClipData.iterator() = object : Iterator<ClipData.Item> {
    private var index = 0
    override fun hasNext() = index < itemCount
    override fun next() = getItemAt(index++) ?: throw IndexOutOfBoundsException()
}

@SuppressLint("ApplySharedPref")
fun SharedPreferences.clear() = edit().clear().commit()

fun Context.getStatusBarHeight(): Int {
    var result = 0
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        result = resources.getDimensionPixelSize(resourceId)
    }
    return result
}

inline val <reified T : Any> Class<T>.TAG: String get() = "$`package`$name"

inline val <reified T : Any> Class<T>.ID: Int get() = TAG.hashCode()

inline val <reified T> ObservableField<T>.value: T?
    get() = this.get()

fun daysOfWeekFromLocale(): Array<DayOfWeek> {
    val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek
    var daysOfWeek = DayOfWeek.values()
    // Order `daysOfWeek` array so that firstDayOfWeek is at index 0.
    if (firstDayOfWeek != DayOfWeek.MONDAY) {
        val rhs = daysOfWeek.sliceArray(firstDayOfWeek.ordinal..daysOfWeek.indices.last)
        val lhs = daysOfWeek.sliceArray(0 until firstDayOfWeek.ordinal)
        daysOfWeek = rhs + lhs
    }
    return daysOfWeek
}

fun View.makeVisible() {
    visibility = View.VISIBLE
}

fun View.makeInvisible() {
    visibility = View.INVISIBLE
}

internal fun TextView.setTextColorRes(@ColorRes color: Int) = setTextColor(context.resources.getColor(color))

fun Spinner.setSpinnerEntries(entries: List<Any>?) {
    if (entries != null) {
        val arrayAdapter = ArrayAdapter(context, R.layout.simple_spinner_item, entries)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        adapter = arrayAdapter
    }
}

/**
 * set spinner onItemSelectedListener listener
 */
fun Spinner.setSpinnerItemSelectedListener(listener: ItemSelectedListener?) {
    if (listener == null) {
        onItemSelectedListener = null
    } else {
        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (tag != position) {
                    listener.onItemSelected(parent.getItemAtPosition(position))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }
}

/**
 * set spinner onItemSelectedListener listener
 */
fun Spinner.setSpinnerInverseBindingListener(listener: InverseBindingListener?) {
    if (listener == null) {
        onItemSelectedListener = null
    } else {
        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (tag != position) {
                    listener.onChange()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }
}

/**
 * set spinner value
 */
fun Spinner.setSpinnerValue(value: Any?) {
    if (adapter != null ) {
        val position = (adapter as ArrayAdapter<Any>).getPosition(value)
        setSelection(position, false)
        tag = position
    }
}

/**
 * get spinner value
 */
fun Spinner.getSpinnerValue(): Any? {
    return selectedItem
}
interface ItemSelectedListener {
    fun onItemSelected(item: Any)
}

fun Toolbar.changeToolbarFont(){
    for (i in 0 until childCount) {
        val view = getChildAt(i)
        if (view is TextView && view.text == title) {
            view.typeface = Typeface.createFromAsset(view.context.assets, "fonts/MADECanvasRegular.otf")
            break
        }
    }
}




