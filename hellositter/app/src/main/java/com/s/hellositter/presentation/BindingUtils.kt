package com.s.hellositter.presentation

import androidx.databinding.BindingAdapter
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.view.View
import android.view.animation.TranslateAnimation
import android.widget.Spinner
import com.s.hellositter.showSoftKeyboard
import android.R
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


@BindingAdapter("gainFocus")
fun gainFocus(editText: EditText, focus: Boolean) {
    if (focus)
        editText.postDelayed({
            editText.requestFocus()
            editText.showSoftKeyboard()
        }, 200)
    else
        editText.clearFocus()
}

interface OnKeyPressedListener {
    fun onKeyPressed()
}

@BindingAdapter("scrollUp")
fun scrollUp(recyclerView: androidx.recyclerview.widget.RecyclerView, position: Int) {
    recyclerView.smoothScrollToPosition(position)
}

@BindingAdapter("onKeyDone")
fun onKeyDone(editText: EditText, action: OnKeyPressedListener) {
    editText.setOnEditorActionListener { _, actionId, event ->
        if (actionId == EditorInfo.IME_ACTION_SEARCH
            || actionId == EditorInfo.IME_ACTION_DONE
            || event?.action == KeyEvent.ACTION_DOWN
            && event.keyCode == KeyEvent.KEYCODE_ENTER
        ) {
            action.onKeyPressed()
            return@setOnEditorActionListener true
        }
        return@setOnEditorActionListener false
    }
}

//@BindingAdapter ("selector")
//fun setSelector (textView: TextView, item: XContentViewModel) {
//    if (item.grayedOut.get() == true)
//        textView.setBackgroundResource(R.drawable.bg_family_selector_gray)
//    else
//        textView.setBackgroundResource(R.drawable.bg_family_selector)
//
//}

@BindingAdapter("shake")
fun shake(view: View, shake: Boolean) =
    TranslateAnimation(-20f, 20f, 0f, 0f).apply {
        this.duration = 200
        this.repeatCount = 2
        this.fillAfter = false
        view.startAnimation(this)
    }

@BindingAdapter("fabAnim")
fun fabAnim(view: View, shake: Boolean) =
    TranslateAnimation(0f, 0f, 200f, 0f).apply {
        this.duration = 1000
        this.repeatCount = 0
        this.fillAfter = false
        view.startAnimation(this)
    }

@BindingAdapter("entries")
fun Spinner.setEntries(entries: List<Any>?) {
    setSpinnerEntries(entries)
}

@BindingAdapter("onItemSelected")
fun Spinner.setOnItemSelectedListener(itemSelectedListener: ItemSelectedListener?) {
    setSpinnerItemSelectedListener(itemSelectedListener)
}

@BindingAdapter("newValue")
fun Spinner.setNewValue(newValue: Any?) {
    setSpinnerValue(newValue)
}

@BindingAdapter("avatar")
fun loadImage(imageView: ImageView, imageURL: String) {
    Glide.with(imageView.getContext())
        .setDefaultRequestOptions(
            RequestOptions()
                .circleCrop()
        )
        .load(imageURL)
        .placeholder(R.drawable.ic_secure)
        .into(imageView)
}

