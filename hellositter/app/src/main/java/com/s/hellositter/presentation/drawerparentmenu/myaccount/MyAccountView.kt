package com.s.hellositter.presentation.drawerparentmenu.myaccount

import com.s.hellositter.presentation.base.BaseView

interface MyAccountView: BaseView {

    fun goToEdit(){}
}