package com.s.hellositter.presentation.mysitters

import android.view.WindowManager
import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.withActivity

class MySittersFragment : BaseFragment<MySittersViewModel>(), MySittersView {

    override fun getLayoutResource(): Int = R.layout.fragment_my_sitters

    override fun getViewModelClass(): Class<MySittersViewModel> = MySittersViewModel::class.java

    override fun viewInit() {
        withActivity<BaseActivity> {
            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
            showBottomNavigation()
            supportActionBar?.show()
        }
    }
}