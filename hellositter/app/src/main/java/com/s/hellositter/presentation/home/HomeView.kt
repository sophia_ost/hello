package com.s.hellositter.presentation.home

import com.s.hellositter.presentation.base.BaseView

interface HomeView : BaseView {

    fun openUsersListFragment()

}
