package com.s.hellositter.presentation.drawerparentmenu.help

import com.s.hellositter.presentation.base.BaseViewModel
import javax.inject.Inject
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.net.Uri
import com.s.hellositter.R


class HelpViewModel @Inject constructor(): BaseViewModel<HelpView>() {

    fun onMessageClick(){
        val sendTo = "mailto:" + view?.context()?.resources?.getString(R.string.help_email)
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.type = "application/octet-stream"
        intent.data = Uri.parse(sendTo)
        view?.call(intent)
    }

    fun onPhoneClick(){
        val intent = Intent(Intent.ACTION_CALL)
        intent.data = Uri.parse("tel:" + view?.context()?.resources?.getString(R.string.help_phone_number))
        view?.call(intent)
    }

    fun onCustomerClick(){

    }
}