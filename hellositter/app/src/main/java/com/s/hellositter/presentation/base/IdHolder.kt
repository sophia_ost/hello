package com.s.hellositter.presentation.base

interface IdHolder {
    var comparableId: String?
}
