package com.s.hellositter.presentation.di

import dagger.Module
import dagger.android.ContributesAndroidInjector

import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.book.BookFragment
import com.s.hellositter.presentation.bookings.BookingsFragment
import com.s.hellositter.presentation.drawerparentmenu.myaccount.childaccount.allergies.AllergiesFragment
import com.s.hellositter.presentation.drawerparentmenu.myaccount.childaccount.specialneeds.SpecialNeedsFragment
import com.s.hellositter.presentation.drawerparentmenu.about.AboutFragment
import com.s.hellositter.presentation.drawerparentmenu.about.faq.FaqFragment
import com.s.hellositter.presentation.drawerparentmenu.about.termsconditions.TermsConditionsFragment
import com.s.hellositter.presentation.drawerparentmenu.address.AddressFragment
import com.s.hellositter.presentation.drawerparentmenu.address.addressnew.AddressNewFragment
import com.s.hellositter.presentation.drawerparentmenu.address.addressnew.AddressNewViewModel
import com.s.hellositter.presentation.drawerparentmenu.help.HelpFragment
import com.s.hellositter.presentation.drawerparentmenu.invitefriends.InviteFriendsFragment
import com.s.hellositter.presentation.drawerparentmenu.myaccount.MyAccountEditFragment
import com.s.hellositter.presentation.drawerparentmenu.myaccount.MyAccountFragment
import com.s.hellositter.presentation.drawerparentmenu.myaccount.childaccount.ChildAccountEditFragment
import com.s.hellositter.presentation.drawerparentmenu.myaccount.childaccount.ChildAccountFragment
import com.s.hellositter.presentation.drawerparentmenu.payment.PaymentEditFragment
import com.s.hellositter.presentation.drawerparentmenu.settings.SettingsFragment
import com.s.hellositter.presentation.drawerparentmenu.payment.PaymentFragment
import com.s.hellositter.presentation.drawerparentmenu.settings.changepass.ChangePassFragment
import com.s.hellositter.presentation.home.HomeFragment
import com.s.hellositter.presentation.intro.intro.IntroFragment
import com.s.hellositter.presentation.mysitters.MySittersFragment
import com.s.hellositter.presentation.users.UsersFragment

@Module
abstract class UiInjectionsModule {

    @ContributesAndroidInjector
    abstract fun injectMain(): BaseActivity

    @ContributesAndroidInjector
    abstract fun injectHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun injectUsersFragment(): UsersFragment

    @ContributesAndroidInjector
    abstract fun injectIntroFragment(): IntroFragment

    @ContributesAndroidInjector
    abstract fun injectAccountFragment(): MyAccountFragment

    @ContributesAndroidInjector
    abstract fun injectAccountEditFragment(): MyAccountEditFragment

    @ContributesAndroidInjector
    abstract fun injectSettingFragment(): SettingsFragment

    @ContributesAndroidInjector
    abstract fun injectPaymentFragment(): PaymentFragment

    @ContributesAndroidInjector
    abstract fun injectInviteFriendsFragment(): InviteFriendsFragment

    @ContributesAndroidInjector
    abstract fun injectHelpFragment(): HelpFragment

    @ContributesAndroidInjector
    abstract fun injectAboutFragment(): AboutFragment

    @ContributesAndroidInjector
    abstract fun injectMySittersFragment(): MySittersFragment

    @ContributesAndroidInjector
    abstract fun injectBookFragment(): BookFragment

    @ContributesAndroidInjector
    abstract fun injectBookingsFragment(): BookingsFragment

    @ContributesAndroidInjector
    abstract fun injectFaqFragment(): FaqFragment

    @ContributesAndroidInjector
    abstract fun injectTermsConditionsFragment(): TermsConditionsFragment

    @ContributesAndroidInjector
    abstract fun injectAddressFragment(): AddressFragment

    @ContributesAndroidInjector
    abstract fun injectChildAccountFragment(): ChildAccountFragment

    @ContributesAndroidInjector
    abstract fun injectChildAccountEditFragment(): ChildAccountEditFragment

    @ContributesAndroidInjector
    abstract fun injectAllergiesFragment(): AllergiesFragment

    @ContributesAndroidInjector
    abstract fun injectSpecialNeedsFragment(): SpecialNeedsFragment

    @ContributesAndroidInjector
    abstract fun injectChangePassFragment(): ChangePassFragment

    @ContributesAndroidInjector
    abstract fun injectAddressNewFragment(): AddressNewFragment

    @ContributesAndroidInjector
    abstract fun injectPaymentEditNewFragment(): PaymentEditFragment






//add

}
