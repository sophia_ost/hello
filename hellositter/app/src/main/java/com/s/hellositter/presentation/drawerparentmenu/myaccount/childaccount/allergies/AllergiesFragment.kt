package com.s.hellositter.presentation.drawerparentmenu.myaccount.childaccount.allergies

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.withActivity

class AllergiesFragment : BaseFragment<AllergiesViewModel>(),
    AllergiesView {

    override fun getLayoutResource(): Int = R.layout.fragment_allergies

    override fun getViewModelClass(): Class<AllergiesViewModel> = AllergiesViewModel::class.java

    override fun viewInit() {
        withActivity<BaseActivity> {
            hideBottomNavigation()
        }
    }
}