package com.s.hellositter.domain.user

import androidx.lifecycle.LifecycleOwner
import io.reactivex.Observable

import com.s.hellositter.domain.model.User


interface UserRepo {

    fun getAll(owner: LifecycleOwner? = null): Observable<List<User>> = Observable.empty()
    fun getAllStatic(): List<User>? = listOf()
    fun save(user: User) {}
    fun deleteAllTable() {}
    fun deleteById(id: String) {}

}
