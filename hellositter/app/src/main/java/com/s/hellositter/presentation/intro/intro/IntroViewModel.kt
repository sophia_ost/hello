package com.s.hellositter.presentation.intro.intro

import androidx.databinding.ObservableField
import com.s.hellositter.presentation.base.BaseViewModel
import java.util.*
import javax.inject.Inject

class IntroViewModel @Inject constructor(): BaseViewModel<IntroView>() {

    var showNext : ObservableField<Boolean> = ObservableField(false)

    fun gotToHome(){
        view?.goToHome()
    }
}