package com.s.hellositter.presentation.drawerparentmenu.about.faq

import com.s.hellositter.presentation.base.BaseFragment

class FaqFragment: BaseFragment<FaqViewModel>(), FaqView {

    override fun getLayoutResource(): Int =1

    override fun getViewModelClass(): Class<FaqViewModel> = FaqViewModel::class.java
}