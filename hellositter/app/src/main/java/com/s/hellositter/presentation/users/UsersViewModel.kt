package com.s.hellositter.presentation.users

import javax.inject.Inject
import android.content.Context

import me.tatarka.bindingcollectionadapter2.collections.DiffObservableList
import com.s.hellositter.presentation.base.GenericDiff
import me.tatarka.bindingcollectionadapter2.OnItemBind

import com.s.hellositter.presentation.base.BaseViewModel
import com.s.hellositter.presentation.base.ItemCLickListener
import com.s.hellositter.presentation.users.list.ContentViewModel
import com.s.hellositter.presentation.base.BaseListItem
import com.s.hellositter.domain.user.UsersInteractor
import com.s.hellositter.default
import com.s.hellositter.toDisposables
import com.s.hellositter.BR
import com.s.hellositter.presentation.custom.onBackground
import com.s.hellositter.domain.model.User

class UsersViewModel @Inject constructor(val context: Context) : BaseViewModel<UsersView>(), ItemCLickListener {

    var items: DiffObservableList<BaseListItem> = DiffObservableList(GenericDiff)
    val onItemBind: OnItemBind<BaseListItem> =
        OnItemBind { itemBinding, position, item ->
            itemBinding.set(
                BR.viewModel,
                item.layoutRes
            )
        }  //BR._all? or BR.item?

    @Inject
    lateinit var interactor: UsersInteractor

    override fun dispose() {
        super.dispose()
        items = DiffObservableList(GenericDiff)
    }

    fun getAll() {
        interactor.getAll(view as androidx.fragment.app.Fragment)
            .default { handleCommonErrors(it) }
            .doOnSubscribe { view?.showProgress() }
            .subscribe({
                it?.apply {
                    items.update(
                        this.map {
                            ContentViewModel(view, it)
                        }
                    )
                }
                view?.hideProgress()
            }, {
                view?.hideProgress()
                it.printStackTrace()
            })
            .toDisposables(disposables)

        addTestInDb()

    }

    override fun onItemClick(item: BaseListItem, id: Int) {
        super.onItemClick(item, id)
    }

    private fun addTestInDb() {
        onBackground {
            interactor.save(User("USER1"))
            interactor.save(User("USER2"))
            interactor.save(User("USER3"))
            interactor.save(User("USER4"))
        }
    }


}
