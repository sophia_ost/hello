package com.s.hellositter.presentation.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import com.s.hellositter.presentation.custom.onUi

open class BaseViewModel<T : BaseView> : ViewModel() {

    var view: T? = null

    protected var disposables = CompositeDisposable()

    open fun dispose() {
        disposables.clear()
        view?.disposePendingActions()
    }

    fun handleCommonErrors(e: Throwable?) {
        e?.printStackTrace()
        onUi {
            e?.apply {
                localizedMessage?.apply {
                    view?.showError(this)
                }
            }
        }
    }

}
