package com.s.hellositter.domain.user

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

import com.s.hellositter.TABLE_USERS
import com.s.hellositter.domain.model.User

@Dao
interface UserDao {

    @Query("SELECT * FROM $TABLE_USERS")
    fun getAll(): LiveData<List<User>>?

    @Query("SELECT * FROM $TABLE_USERS")
    fun getAllStatic(): List<User>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(vararg item: User)

    @Query("DELETE FROM $TABLE_USERS")
    fun deleteAllTable()

    @Query("DELETE FROM USERS WHERE id = :id")
    fun deleteById(id: String)

}
