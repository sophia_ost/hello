package com.s.hellositter.domain.model

open class BaseResponse<T> {

    val data: T? = null
    val error: BaseError? = null

}