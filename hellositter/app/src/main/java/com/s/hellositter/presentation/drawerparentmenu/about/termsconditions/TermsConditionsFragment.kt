package com.s.hellositter.presentation.drawerparentmenu.about.termsconditions

import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.base.BaseView

class TermsConditionsFragment: BaseFragment<TermsConditionsViewModel>(), BaseView {

    override fun getLayoutResource(): Int =1

    override fun getViewModelClass(): Class<TermsConditionsViewModel> = TermsConditionsViewModel::class.java
}