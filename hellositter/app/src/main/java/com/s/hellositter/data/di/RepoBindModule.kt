package com.s.hellositter.data.di

import dagger.Binds
import dagger.Module

import com.s.hellositter.domain.user.UserRepo
import com.s.hellositter.data.user.UserRepoDatabase

@Module
abstract class RepoBindModule {

    @Binds
    @User(USER_DATABASE)
    abstract fun provideDatabaseMyRepo(restRepo: UserRepoDatabase): UserRepo

}
