package com.s.hellositter.presentation.users

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseFragment


class UsersFragment : BaseFragment<UsersViewModel>(), UsersView {

    override fun getViewModelClass(): Class<UsersViewModel> = UsersViewModel::class.java
    override fun getLayoutResource(): Int = R.layout.fragment_users

    override fun viewInit() {
        addBackArrow(true)
        viewModel?.getAll()
    }


}
