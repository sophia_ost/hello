package com.s.hellositter.data.di

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

import com.s.hellositter.App
import com.s.hellositter.BuildConfig
import com.s.hellositter.PREF_NAME
import io.swagger.client.api.PaymentControllerApi
import io.swagger.client.model.*

@Module
class BaseModule {

    @Provides
    @Singleton
    fun provideApp(): App = App.get()

    @Provides
    @Singleton
    fun provideContext(app: App): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);


    @Provides
    @Rest(REST_SERVER)
    @Singleton
    internal fun provideApiRetrofit(@Rest(REST_SERVER) okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.BASE_ENDPOINT)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

//    @Provides
//    @Rest(REST_GENERAL)
//    @Singleton
//    internal fun provideApiRetrofitGeneral(): Retrofit = Retrofit.Builder()
//        .client(OkHttpClient.Builder()
//            .addInterceptor {
//                //here you can parse the response explicitly
//                return@addInterceptor it.proceed(it.request())
//            }
//            .addInterceptor(HttpLoggingInterceptor().setLevel(BuildConfig.DEBUG({ HttpLoggingInterceptor.Level.BODY },
//                { HttpLoggingInterceptor.Level.NONE })))
//            .build())
//        .baseUrl(BuildConfig.BASE_ENDPOINT)
//        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//        .addConverterFactory(GsonConverterFactory.create())
//        .build()



}

