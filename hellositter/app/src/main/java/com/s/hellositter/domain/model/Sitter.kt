package com.s.hellositter.domain.model

data class Sitter(
    var name: String?,
    var lastName: String?,
    var password: String?,
    var email: String?,
    var phoneNumber: String?,
    var driversPhoto: String?,
    var socialSecurity: String?,
    var references: List<String>?,
    var cprCertificate: List<String>?,
    var interviewTime: String?,
    var carInsurancePhoto: String?,
    var kidsToWatch: Int?,
    var watchSpecialNeeds: Boolean?,
    var petAllergies: String?,
    var myPhoto: List<String>?,
    var myVideo: String?,
    var myQualities: List<String>,
    var school: String?,
    var speciality: String?,
    var specialDegreePhoto: String?,
    var specialDegreeReference: String?,
    var specialDegreePhone: String?,
    var payment: String?
)