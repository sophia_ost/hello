package com.s.hellositter.presentation.drawerparentmenu.myaccount

import com.s.hellositter.BR
import com.s.hellositter.R
import com.s.hellositter.domain.model.Child
import com.s.hellositter.presentation.base.BaseListItem
import com.s.hellositter.presentation.base.BaseViewModel
import com.s.hellositter.presentation.base.GenericDiff
import com.s.hellositter.presentation.base.ItemCLickListener
import com.s.hellositter.presentation.drawerparentmenu.myaccount.list.ContentChildViewModel
import me.tatarka.bindingcollectionadapter2.OnItemBind
import me.tatarka.bindingcollectionadapter2.collections.DiffObservableList
import javax.inject.Inject

class MyAccountViewModel @Inject constructor() : BaseViewModel<MyAccountView>(), ItemCLickListener {

    var items: DiffObservableList<BaseListItem> = DiffObservableList(GenericDiff)

    val onItemBind: OnItemBind<BaseListItem> =
        OnItemBind { itemBinding, position, item -> itemBinding.set(BR.item, item.layoutRes) }

    var child1: Child = Child(
        "first",
        view?.context()?.getDrawable(R.drawable.burger),
        "",
        "",
        "",
        "",
        "",
        1
    )
    var child2: Child = Child(
        "second",
        view?.context()?.getDrawable(R.drawable.ice_cream),
        "",
        "",
        "",
        "",
        "",
        1
    )

    fun setChildren() {
        val itemsAll = listOf(child1, child2)
        items.update(itemsAll.map {
            ContentChildViewModel(view, it, this@MyAccountViewModel)
        })


    }

    fun edit(){
        view?.goToEdit()
    }


}