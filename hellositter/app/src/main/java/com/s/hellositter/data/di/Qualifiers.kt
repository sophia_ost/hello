package com.s.hellositter.data.di

import javax.inject.Qualifier

typealias UserQualifier = String

typealias RestQualifier = String

const val REST_SERVER: RestQualifier = "server rest"

const val REST_GENERAL: RestQualifier = "general"

//const val USER_REST: UserQualifier = "user rest"
const val USER_DATABASE: UserQualifier = "user database"

@Qualifier
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class User(val value: UserQualifier = "")

@Qualifier
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class Rest(val value: String = "")

