package com.s.hellositter.presentation.book

import android.annotation.TargetApi
import android.app.TimePickerDialog
import android.content.Context
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.core.view.children
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import com.kizitonwose.calendarview.utils.yearMonth
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.CalendarMode
import com.s.hellositter.R
import com.s.hellositter.presentation.*
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.setTextColorRes
import kotlinx.android.synthetic.main.calendar_day.*
import kotlinx.android.synthetic.main.fragment_book.*
import kotlinx.android.synthetic.main.item_calendar_day.view.*
import org.threeten.bp.YearMonth
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

class BookFragment : BaseFragment<BookViewModel>(), BookView {

    override fun getLayoutResource(): Int = R.layout.fragment_book

    override fun getViewModelClass(): Class<BookViewModel> = BookViewModel::class.java

    override fun viewInit() {
        viewModel?.setChildren()
        withActivity<BaseActivity> {
            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
            showBottomNavigation()
            supportActionBar?.show()
        }
        trycal()

    }
    private var selectedDate: org.threeten.bp.LocalDate? = null
    private val today = org.threeten.bp.LocalDate.now()


    fun trycal() {
        val daysOfWeek = daysOfWeekFromLocale()
        legendLayout.children.forEachIndexed { index, view ->
            (view as TextView).apply {
                text = daysOfWeek[index].name.take(3)
            }
        }

        val currentMonth = YearMonth.now()
        val endMonth = currentMonth.plusMonths(1)
        calendar_view.setup(currentMonth, endMonth, daysOfWeek.first())
        calendar_view.scrollToMonth(currentMonth)
        calendar_view.scrollToDate(today)

        class DayViewContainer(view: View) : ViewContainer(view) {
            // Will be set when this container is bound. See the dayBinder.
            lateinit var day: com.kizitonwose.calendarview.model.CalendarDay
            val textView = view.exOneDayText

            init {
                view.setOnClickListener {
                    if (selectedDate == day.date) {
                        selectedDate = null
                        calendar_view.notifyDayChanged(day)
                    } else {
                        val oldDate = selectedDate
                        selectedDate = day.date
                        calendar_view.notifyDateChanged(day.date)
                        oldDate?.let { calendar_view.notifyDateChanged(oldDate) }
                    }
                }
            }
        }

        calendar_view.dayBinder = object : DayBinder<DayViewContainer> {
            override fun create(view: View) = DayViewContainer(view)
            override fun bind(container: DayViewContainer, day: com.kizitonwose.calendarview.model.CalendarDay) {
                container.day = day
                val textView = container.textView
                textView.text = day.date.dayOfMonth.toString()

                if (day.owner == DayOwner.THIS_MONTH) {
                    textView.makeVisible()
                    when (day.date) {
                        selectedDate -> {
                            textView.setTextColorRes(R.color.active_dot)
                            textView.setBackgroundResource(R.drawable.bg_selected_day)
                        }
                        today -> {
                            textView.setTextColorRes(R.color.active_dot)
                            textView.setBackgroundResource(R.drawable.inactive_dots)
                        }
                        else -> {
                            textView.setTextColorRes(R.color.active_dot)
                            textView.background = null
                        }
                    }
                } else {
                    textView.makeInvisible()
                }
            }
        }

    }



}

