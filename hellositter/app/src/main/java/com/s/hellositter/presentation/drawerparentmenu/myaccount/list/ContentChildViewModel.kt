package com.s.hellositter.presentation.drawerparentmenu.myaccount.list

import com.s.hellositter.R
import com.s.hellositter.domain.model.Child
import com.s.hellositter.presentation.base.BaseListItem
import com.s.hellositter.presentation.base.ItemCLickListener
import com.s.hellositter.presentation.drawerparentmenu.myaccount.MyAccountView

class ContentChildViewModel(val view: MyAccountView?, val item: Child, val listener: ItemCLickListener):
    BaseListItem {

    override val layoutRes: Int = R.layout.item_account_child
    override var comparableId: String? =""


}