package com.s.hellositter.presentation.book

import android.app.TimePickerDialog
import android.content.Context
import androidx.databinding.ObservableField
import com.s.hellositter.BR
import com.s.hellositter.R
import com.s.hellositter.domain.model.Child
import com.s.hellositter.presentation.base.BaseListItem
import com.s.hellositter.presentation.base.BaseViewModel
import com.s.hellositter.presentation.base.GenericDiff
import com.s.hellositter.presentation.base.ItemCLickListener
import com.s.hellositter.presentation.book.list.ChildContentViewModel
import me.tatarka.bindingcollectionadapter2.OnItemBind
import me.tatarka.bindingcollectionadapter2.collections.DiffObservableList
import java.lang.reflect.Array
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class BookViewModel @Inject constructor() : BaseViewModel<BookView>(), ItemCLickListener {

    var items: DiffObservableList<BaseListItem> = DiffObservableList(GenericDiff)

    val onItemBind: OnItemBind<BaseListItem> =
        OnItemBind { itemBinding, position, item -> itemBinding.set(BR.item, item.layoutRes) }


    var timeStart: ObservableField<String> = ObservableField("")
    var timeEnd: ObservableField<String> = ObservableField("")
    val cal = Calendar.getInstance()
    var child1: Child = Child(
        "first",
        view?.context()?.getDrawable(R.drawable.burger),
        "",
        "",
        "",
        "",
        "",
        1
    )
    var child2: Child = Child(
        "second",
        view?.context()?.getDrawable(R.drawable.ice_cream),
        "",
        "",
        "",
        "",
        "",
        1
    )

    val entriesNew = listOf("first add", "second add", "new add")
    fun onSelectStartClicked() {
        val timePickerDialog = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            var h = hour
            var state = "am"
            if (hour>12){
                h-=12
                state = "pm"
            }
            cal.set(Calendar.HOUR_OF_DAY, h)
            cal.set(Calendar.MINUTE, minute)
            timeStart.set(SimpleDateFormat("HH:mm").format(cal.time)+ state)
        }
        showTimer(timePickerDialog)
    }

    fun onSelectEndClicked() {
        val timePickerDialog = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            var h = hour
            var state = "am"
            if (hour>12){
                h-=12
                state = "pm"
            }
            cal.set(Calendar.HOUR_OF_DAY, h)
            cal.set(Calendar.MINUTE, minute)
            timeEnd.set(SimpleDateFormat("HH:mm").format(cal.time)+state)
        }
        showTimer(timePickerDialog)
    }

    fun showTimer( timePickerDialog: TimePickerDialog.OnTimeSetListener){
        TimePickerDialog(
            view?.context(),
            timePickerDialog,
            cal.get(Calendar.HOUR_OF_DAY),
            cal.get(Calendar.MINUTE),
            false
        ).show()
    }


    fun setChildren() {
        val itemsAll = listOf(child1, child2)
        items.update(itemsAll.map {
            ChildContentViewModel(view, it, this@BookViewModel) })


    }


}