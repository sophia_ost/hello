package com.s.hellositter.presentation.drawerparentmenu.invitefriends

import android.content.Intent
import androidx.core.content.ContextCompat
import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.navigate
import com.s.hellositter.presentation.permissionCheck
import com.s.hellositter.presentation.withActivity
import kotlinx.android.synthetic.main.activity_base.*

class InviteFriendsFragment : BaseFragment<InviteFriendsViewModel>(), InviteFriendsView {
    override fun getLayoutResource(): Int = R.layout.fragment_drawer_invite_friends

    override fun getViewModelClass(): Class<InviteFriendsViewModel> = InviteFriendsViewModel::class.java

    override fun viewInit() {
        withActivity<BaseActivity> {
            hideBottomNavigation()

        }
    }

    override fun navigateBack() {
        this@InviteFriendsFragment.navigate(R.id.action_invite_friends_fragment_to_book_tab)
    }

    override fun send(intent: Intent) {
        withActivity<BaseActivity> {
            permissionCheck(android.Manifest.permission.SEND_SMS) {
                if (this) {
                    this@InviteFriendsFragment.startActivity(intent)
                }
            }
        }

    }

    override fun sendMail(intent: Intent) {
        this@InviteFriendsFragment.startActivity(Intent.createChooser(intent, "Send mail..."))
    }
}