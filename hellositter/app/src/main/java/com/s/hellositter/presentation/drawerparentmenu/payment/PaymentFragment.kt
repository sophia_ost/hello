package com.s.hellositter.presentation.drawerparentmenu.payment

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.navigate
import com.s.hellositter.presentation.withActivity
import kotlinx.android.synthetic.main.activity_base.*

class PaymentFragment: BaseFragment<PaymentViewModel>(), PaymentView {

    override fun getLayoutResource(): Int = R.layout.fragment_drawer_payment

    override fun getViewModelClass(): Class<PaymentViewModel> = PaymentViewModel::class.java

    override fun viewInit() {
        withActivity<BaseActivity> {
            hideBottomNavigation()

        }
    }

    override fun navigateBack() {
        this@PaymentFragment.navigate(R.id.action_payment_fragment_to_book_tab)
    }
}