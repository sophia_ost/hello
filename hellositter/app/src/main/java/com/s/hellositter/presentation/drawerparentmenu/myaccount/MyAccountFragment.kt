package com.s.hellositter.presentation.drawerparentmenu.myaccount

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.navigate
import com.s.hellositter.presentation.withActivity
import kotlinx.android.synthetic.main.activity_base.*

class MyAccountFragment: BaseFragment<MyAccountViewModel>(), MyAccountView {

    override fun getLayoutResource(): Int = R.layout.fragment_drawer_account

    override fun getViewModelClass(): Class<MyAccountViewModel> = MyAccountViewModel::class.java

    override fun viewInit() {
        withActivity<BaseActivity> {
            hideBottomNavigation()
        }

        viewModel?.setChildren()
    }

    override fun navigateBack() {
        this@MyAccountFragment.navigate(R.id.action_account_fragment_to_book_tab)
    }

    override fun goToEdit(){
        this.navigate(R.id.action_account_fragment_to_myAccountEditFragment)
    }
}