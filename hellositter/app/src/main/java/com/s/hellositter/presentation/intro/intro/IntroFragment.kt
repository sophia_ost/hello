package com.s.hellositter.presentation.intro.intro


import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.ViewPager
import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.navigate
import com.s.hellositter.presentation.withActivity

class IntroFragment: BaseFragment<IntroViewModel>(), IntroView {

    lateinit var pager: ViewPager

    var layouts: IntArray = intArrayOf(R.layout.slide_one, R.layout.slide_two, R.layout.slide_three)

    lateinit var dotsLayout: LinearLayout

    lateinit var dots: Array<ImageView>

    lateinit var adapter: IntroViewPagerAdapter

    override fun getLayoutResource(): Int = R.layout.fragment_intro

    override fun getViewModelClass(): Class<IntroViewModel> = IntroViewModel::class.java

    override fun viewInit() {
        withActivity<BaseActivity> {
            activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
            hideBottomNavigation()
            supportActionBar?.hide()
            pager = findViewById(R.id.pager)
            adapter = IntroViewPagerAdapter(layouts, context)
            pager.adapter = adapter
            dotsLayout = findViewById(R.id.dots)
        }
        createDots(0)
        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                createDots(position)
                if (position == layouts.size-1){
                    viewModel?.showNext?.set(true)
                } else {
                    viewModel?.showNext?.set(false)
                }
            }

        })

    }

    fun createDots(position: Int){
        if (dotsLayout!=null){
            dotsLayout.removeAllViews()
        }
        dots = Array(layouts.size){i -> ImageView(context)}

        for (i in layouts.indices){
            dots[i] =ImageView(context)
            if (i==position){
                dots[i].setImageDrawable(resources.getDrawable(R.drawable.active_dots))
            } else{
                dots[i].setImageDrawable(resources.getDrawable(R.drawable.inactive_dots))
            }
            val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            params.setMargins(8,0,8,0)
            dotsLayout.addView(dots[i], params)
        }



    }



    override fun goToHome(){
        this.navigate(R.id.action_introFragment_to_book_tab)
    }



}