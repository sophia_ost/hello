package com.s.hellositter.presentation.auth.zip

import com.s.hellositter.presentation.base.BaseFragment

class ZipFragment: BaseFragment<ZipViewModel>(), ZipView {

    override fun getLayoutResource(): Int =1

    override fun getViewModelClass(): Class<ZipViewModel> = ZipViewModel::class.java
}