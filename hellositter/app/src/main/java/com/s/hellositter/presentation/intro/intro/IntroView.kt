package com.s.hellositter.presentation.intro.intro

import com.s.hellositter.presentation.base.BaseView

interface IntroView: BaseView {

    fun goToHome()
}