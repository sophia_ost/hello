package com.s.hellositter.presentation.home

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.withActivity

class HomeFragment : BaseFragment<HomeViewModel>(), HomeView {

    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java
    override fun getLayoutResource(): Int = R.layout.fragment_home

    override fun viewInit() {
        withActivity<BaseActivity> {
            addBackArrow(false)
            supportActionBar?.show()
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
//            supportActionBar?.setHomeButtonEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)

        }

    }




    override fun openUsersListFragment() {}


}
