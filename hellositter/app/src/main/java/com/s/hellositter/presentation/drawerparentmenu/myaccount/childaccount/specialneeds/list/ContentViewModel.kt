package com.s.hellositter.presentation.drawerparentmenu.myaccount.childaccount.specialneeds.list

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseListItem

class ContentViewModel(val familyName: String) : BaseListItem {


    override val layoutRes = R.layout.item_special_needs

    override var comparableId: String? = ""
}