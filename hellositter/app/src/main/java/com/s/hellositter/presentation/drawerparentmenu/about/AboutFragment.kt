package com.s.hellositter.presentation.drawerparentmenu.about

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.navigate
import com.s.hellositter.presentation.withActivity
import kotlinx.android.synthetic.main.activity_base.*

class AboutFragment : BaseFragment<AboutViewModel>(), AboutView {
    override fun getLayoutResource(): Int = R.layout.fragment_drawer_about

    override fun getViewModelClass(): Class<AboutViewModel> = AboutViewModel::class.java

    override fun viewInit() {
        withActivity<BaseActivity> {
            hideBottomNavigation()

        }
    }

    override fun navigateBack() {
        this@AboutFragment.navigate(R.id.action_about_fragment_to_book_tab)
    }
}