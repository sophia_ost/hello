package com.s.hellositter.data.user

import javax.inject.Inject
import javax.inject.Singleton

import com.s.hellositter.domain.user.UserRepo
import com.s.hellositter.data.base.BaseRepo


@Singleton
class UserRepoRest @Inject constructor() : BaseRepo(), UserRepo {

}

