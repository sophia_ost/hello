package com.s.hellositter.presentation.base

import android.content.Context

interface BaseView {

    fun showError(message: String?, title: String? = null): Unit?

    fun showMessage(message: String, title: String? = null): Unit?

    fun showSnackBar(message: String?): Unit?

    fun showProgress()

    fun hideProgress()

    fun viewInit() {}

    fun beforeViewInit() {}

    fun disposePendingActions() {}

    fun navigateBack() {}

    fun context(): Context?

}
