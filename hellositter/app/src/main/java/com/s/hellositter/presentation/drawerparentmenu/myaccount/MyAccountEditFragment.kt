package com.s.hellositter.presentation.drawerparentmenu.myaccount

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseFragment

class MyAccountEditFragment: BaseFragment<MyAccountViewModel>(), MyAccountView {

    override fun getLayoutResource(): Int = R.layout.fragment_drawer_account_edit

    override fun getViewModelClass(): Class<MyAccountViewModel> = MyAccountViewModel::class.java

    override fun viewInit() {
        viewModel?.setChildren()
    }


}