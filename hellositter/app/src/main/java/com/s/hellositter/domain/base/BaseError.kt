package com.s.hellositter.domain.model

data class BaseError(val message: String?,
                     val code: String?)