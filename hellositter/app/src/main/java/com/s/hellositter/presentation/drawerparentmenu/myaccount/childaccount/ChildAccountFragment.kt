package com.s.hellositter.presentation.drawerparentmenu.myaccount.childaccount

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.withActivity

class ChildAccountFragment: BaseFragment<ChildAccountViewModel>(), ChildAccountView {

    override fun getLayoutResource(): Int = R.layout.fragment_child_profile

    override fun getViewModelClass(): Class<ChildAccountViewModel> =ChildAccountViewModel::class.java

    override fun viewInit() {
        withActivity<BaseActivity> {
            hideBottomNavigation()
        }
    }
}