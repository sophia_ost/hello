package com.s.hellositter.presentation.home

import javax.inject.Inject
import com.s.hellositter.presentation.base.BaseViewModel

class HomeViewModel @Inject constructor() : BaseViewModel<HomeView>() {

    fun openUsersListFragment() = view?.openUsersListFragment()

}
