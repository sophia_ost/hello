package com.s.hellositter.presentation.drawerparentmenu.payment

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseFragment

class PaymentEditFragment: BaseFragment<PaymentViewModel>(), PaymentView {

    override fun getLayoutResource(): Int = R.layout.fragment_payment_edit

    override fun getViewModelClass(): Class<PaymentViewModel> = PaymentViewModel::class.java
}