package com.s.hellositter.presentation.drawerparentmenu.payment

import androidx.databinding.ObservableField
import com.s.hellositter.presentation.base.BaseViewModel
import javax.inject.Inject

class PaymentViewModel @Inject constructor(): BaseViewModel<PaymentView>() {

    var isCardAdded : ObservableField<Boolean> = ObservableField(true)
}