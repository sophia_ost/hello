package com.s.hellositter.domain.user

import androidx.lifecycle.LifecycleOwner
import io.reactivex.Observable
import javax.inject.Inject

import com.s.hellositter.data.di.USER_DATABASE
import com.s.hellositter.domain.model.User


class UsersInteractor @Inject constructor() {

    @Inject
    @field:com.s.hellositter.data.di.User(USER_DATABASE)
    lateinit var repoDb: UserRepo

    fun getAll(owner: LifecycleOwner): Observable<List<User>> = repoDb.getAll(owner)
    fun getAllStatic(): List<User>? = repoDb.getAllStatic()
    fun save(user: User) = repoDb.save(user)
    fun deleteAllTable() = repoDb.deleteAllTable()
    fun deleteById(id: String) = repoDb.deleteById(id)


}
