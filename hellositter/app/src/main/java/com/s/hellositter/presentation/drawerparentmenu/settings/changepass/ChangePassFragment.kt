package com.s.hellositter.presentation.drawerparentmenu.settings.changepass

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.navigate

class ChangePassFragment : BaseFragment<ChangePassViewModel>(), ChangePassView {

    override fun getLayoutResource(): Int = R.layout.fragment_change_pass

    override fun getViewModelClass(): Class<ChangePassViewModel> = ChangePassViewModel::class.java

    override fun returnToSettings() {
        navigateBack()
    }

    override fun navigateBack() {
        this.navigate(R.id.action_changePassFragment_to_settings_fragment)
    }
}