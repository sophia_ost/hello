package com.s.hellositter.presentation.base

import android.content.Context
import android.graphics.Typeface
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.ViewDataBinding
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatDelegate
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.navigation.Navigation.findNavController
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject
import kotlinx.android.synthetic.main.activity_base.*
import com.s.hellositter.R
import androidx.navigation.NavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.core.view.GravityCompat
import androidx.navigation.Navigation
import androidx.navigation.ui.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


class BaseActivity : DaggerAppCompatActivity(), BaseView, Bindable {

    private lateinit var drawerLayout: DrawerLayout
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController
    lateinit var bottomNav: BottomNavigationView
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var mainViewModel: MainViewModel

    override var binding: ViewDataBinding? = null

    private fun getLayoutResource(): Int = R.layout.activity_base

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        bind(getLayoutResource(), mainViewModel)
        navController = findNavController(this, R.id.content)
        setSupportActionBar(toolbar)
        setupNavigationDrawer(navController)
        setupBottomNavigation(navController)
        mainViewModel.view = this

    }

    fun setupNavigationDrawer(navController: NavController) {
        drawerLayout = content_wrapper
        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.book_tab, R.id.bookings_tab, R.id.my_sitters_tab),
        //    navController.graph,
            drawerLayout
        )
        toolbar.changeToolbarFont()
        toolbar.setupWithNavController(navController, appBarConfiguration)
        navigationView.setupWithNavController(navController)

    }

    fun setupBottomNavigation(navController: NavController){
        bottomNav = bottom_navigation
        bottomNav.setupWithNavController(navController)
        bottomNav.itemIconTintList = null
        bottomNav.selectedItemId = R.id.book_tab
        navigationView.setupWithNavController(navController)

    }

    fun hideBottomNavigation() {
        bottomNav = bottom_navigation
        bottomNav.visibility = View.GONE
    }

    fun showBottomNavigation() {
        bottomNav = bottom_navigation
        bottomNav.visibility = View.VISIBLE
    }

    override fun onBackPressed() {
        val currentFragment = content.childFragmentManager.fragments[0]
        val controller = Navigation.findNavController(this, R.id.content)
        if (content_wrapper.isDrawerOpen(GravityCompat.START)) {
            content_wrapper.closeDrawer(GravityCompat.START)
        }
        if (currentFragment is BaseView)
            (currentFragment as BaseView).navigateBack()
        else if (!controller.popBackStack())
            super.onBackPressed()

    }


    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base!!))
    }


    override fun onSupportNavigateUp(): Boolean =
        findNavController(this, R.id.content).navigateUp(appBarConfiguration)


    override fun showError(message: String?, title: String?) = showSnackBar(message)

    override fun showMessage(message: String, title: String?): Unit? = showSnackBar(message)

    override fun showSnackBar(message: String?): Unit? =
        binding?.root?.run {
            if (message != null) Snackbar.make(
                this,
                message,
                Snackbar.LENGTH_SHORT
            ).show()
        }

    override fun showProgress() {
        //   mainViewModel.progress.set(true)
    }

    override fun hideProgress() {
        //  mainViewModel.progress.set(false)
    }

    override fun context(): Context? = applicationContext

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val navController = findNavController(this, R.id.content)
       // return item.onNavDestinationSelected(navController) || super.onOptionsItemSelected(item)
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }




}

private fun Toolbar.changeToolbarFont() {
    for (i in 0 until childCount) {
        val view = getChildAt(i)
        if (view is TextView && view.text == title) {
            view.typeface = Typeface.createFromAsset(view.context.assets, "fonts/MADECanvasRegular.otf")
            break
        }
    }
}


