package com.s.hellositter.presentation.drawerparentmenu.settings.changepass

import com.s.hellositter.presentation.base.BaseView

interface ChangePassView: BaseView {

    fun returnToSettings()
}