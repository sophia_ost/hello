package com.s.hellositter.presentation.users.list

import com.s.hellositter.R
import com.s.hellositter.domain.model.User
import com.s.hellositter.presentation.base.BaseListItem
import com.s.hellositter.presentation.users.UsersView

data class ContentViewModel(val view: UsersView?, val item: User) : BaseListItem {

    override var comparableId: String? = item.id.toString()
    override val layoutRes: Int = R.layout.item_user_content


}
