package com.s.hellositter.presentation.base

interface ItemCLickListener {
    fun onItemClick(item: BaseListItem, id: Int) {}
    fun onItemClick(item: BaseListItem, id: Long) {}
    fun onItemClick(item: BaseListItem, id: String) {}


}
