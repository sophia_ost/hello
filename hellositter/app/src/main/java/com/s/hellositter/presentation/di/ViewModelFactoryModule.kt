package com.s.hellositter.presentation.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.s.hellositter.presentation.base.MainViewModel
import com.s.hellositter.presentation.book.BookViewModel
import com.s.hellositter.presentation.bookings.BookingsViewModel
import com.s.hellositter.presentation.drawerparentmenu.myaccount.childaccount.allergies.AllergiesViewModel
import com.s.hellositter.presentation.drawerparentmenu.myaccount.childaccount.specialneeds.SpecialNeedsViewModel
import com.s.hellositter.presentation.drawerparentmenu.about.AboutViewModel
import com.s.hellositter.presentation.drawerparentmenu.about.faq.FaqViewModel
import com.s.hellositter.presentation.drawerparentmenu.about.termsconditions.TermsConditionsViewModel
import com.s.hellositter.presentation.drawerparentmenu.address.AddressViewModel
import com.s.hellositter.presentation.drawerparentmenu.address.addressnew.AddressNewViewModel
import com.s.hellositter.presentation.drawerparentmenu.help.HelpViewModel
import com.s.hellositter.presentation.drawerparentmenu.invitefriends.InviteFriendsViewModel
import com.s.hellositter.presentation.drawerparentmenu.myaccount.MyAccountViewModel
import com.s.hellositter.presentation.drawerparentmenu.myaccount.childaccount.ChildAccountViewModel
import com.s.hellositter.presentation.drawerparentmenu.settings.SettingsViewModel
import com.s.hellositter.presentation.drawerparentmenu.payment.PaymentViewModel
import com.s.hellositter.presentation.drawerparentmenu.settings.changepass.ChangePassViewModel
import com.s.hellositter.presentation.home.HomeViewModel
import com.s.hellositter.presentation.intro.intro.IntroViewModel
import com.s.hellositter.presentation.mysitters.MySittersViewModel
import com.s.hellositter.presentation.users.UsersViewModel


@Module
internal abstract class ViewModelFactoryModule {

    @Binds
    internal abstract fun bindViewModelFactory(viewModelFactory: DaggerViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun bindUserViewModel(myViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun bindHomeViewModel(myViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UsersViewModel::class)
    internal abstract fun bindUsersViewModel(myViewModel: UsersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(IntroViewModel::class)
    internal abstract fun bindIntroViewModel(myViewModel: IntroViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PaymentViewModel::class)
    internal abstract fun bindPaymentViewModel(myViewModel: PaymentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MyAccountViewModel::class)
    internal abstract fun bindAccountViewModel(myViewModel: MyAccountViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    internal abstract fun bindSettingsViewModel(myViewModel: SettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InviteFriendsViewModel::class)
    internal abstract fun bindInviteFriendsViewModel(myViewModel: InviteFriendsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HelpViewModel::class)
    internal abstract fun bindHelpViewModel(myViewModel: HelpViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AboutViewModel::class)
    internal abstract fun bindAboutViewModel(myViewModel: AboutViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BookingsViewModel::class)
    internal abstract fun bindBookingsViewModel(myViewModel: BookingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MySittersViewModel::class)
    internal abstract fun bindMySittersViewModel(myViewModel: MySittersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BookViewModel::class)
    internal abstract fun bindBookViewModel(myViewModel: BookViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FaqViewModel::class)
    internal abstract fun bindFaqViewModel(myViewModel: FaqViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TermsConditionsViewModel::class)
    internal abstract fun bindTermsViewModel(myViewModel: TermsConditionsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddressViewModel::class)
    internal abstract fun bindAddressViewModel(myViewModel: AddressViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChildAccountViewModel::class)
    internal abstract fun bindChildAccountViewModel(myViewModel: ChildAccountViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AllergiesViewModel::class)
    internal abstract fun bindAllergiesViewModel(myViewModel: AllergiesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SpecialNeedsViewModel::class)
    internal abstract fun bindSpecialNeedsViewModel(myViewModel: SpecialNeedsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChangePassViewModel::class)
    internal abstract fun bindChangePassViewModel(myViewModel: ChangePassViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddressNewViewModel::class)
    internal abstract fun bindAddressNewViewModel(myViewModel: AddressNewViewModel): ViewModel



//add
}
