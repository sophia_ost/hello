package com.s.hellositter.presentation.drawerparentmenu.myaccount.childaccount

import com.s.hellositter.presentation.base.BaseViewModel
import javax.inject.Inject

class ChildAccountViewModel @Inject constructor(): BaseViewModel<ChildAccountView>() {
}