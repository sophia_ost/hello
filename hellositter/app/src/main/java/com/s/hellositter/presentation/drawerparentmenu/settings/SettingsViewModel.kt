package com.s.hellositter.presentation.drawerparentmenu.settings

import com.s.hellositter.presentation.base.BaseView
import com.s.hellositter.presentation.base.BaseViewModel
import javax.inject.Inject

class SettingsViewModel @Inject constructor(): BaseViewModel<SettingsView>() {

    fun onChangePassClicked(){
        view?.changePassClicked()
    }
}