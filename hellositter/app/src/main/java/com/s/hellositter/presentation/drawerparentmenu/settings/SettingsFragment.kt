package com.s.hellositter.presentation.drawerparentmenu.settings

import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.navigate
import com.s.hellositter.presentation.withActivity
import kotlinx.android.synthetic.main.activity_base.*
import kotlinx.android.synthetic.main.fragment_drawer_settings.*

class SettingsFragment: BaseFragment<SettingsViewModel>(), SettingsView {

    override fun getLayoutResource(): Int = R.layout.fragment_drawer_settings

    override fun getViewModelClass(): Class<SettingsViewModel> = SettingsViewModel::class.java

    override fun viewInit() {
        withActivity<BaseActivity> {
            hideBottomNavigation()

        }
    }

    fun tryListen(){
    }

    override fun navigateBack() {
        this@SettingsFragment.navigate(R.id.action_settings_fragment_to_book_tab)
    }
    override fun changePassClicked() {
        this.navigate(R.id.action_settings_fragment_to_changePassFragment)
    }
}