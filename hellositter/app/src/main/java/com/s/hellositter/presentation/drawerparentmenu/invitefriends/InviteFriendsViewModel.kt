package com.s.hellositter.presentation.drawerparentmenu.invitefriends

import com.s.hellositter.presentation.base.BaseViewModel
import javax.inject.Inject
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import android.provider.Telephony
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context.CLIPBOARD_SERVICE
import android.widget.Toast
import com.s.hellositter.R


class InviteFriendsViewModel @Inject constructor() : BaseViewModel<InviteFriendsView>() {

    var code: String? = "some code"


    fun onSmsClicked() {
        val defaultSmsPackageName =
            Telephony.Sms.getDefaultSmsPackage(view?.context()) //Need to change the build to API 19

        val sendIntent = Intent(Intent.ACTION_SEND)
        sendIntent.type = "text/plain"
        sendIntent.putExtra(Intent.EXTRA_TEXT, "text for try")

        if (defaultSmsPackageName != null) {
            sendIntent.setPackage(defaultSmsPackageName)
        }
        view?.send(sendIntent)
    }

    fun onMailClicked() {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "application/octet-stream"
        intent.putExtra(Intent.EXTRA_TEXT, "lets see how it works")
        view?.send(intent)
    }

    fun onFacebookClicked() {
        val sendIntent = Intent(Intent.ACTION_SEND)
        sendIntent.putExtra(Intent.EXTRA_TEXT, "My own text \n https://stackoverflow.com/")
        sendIntent.type = "text/plain"
        sendIntent.setPackage("com.facebook.orca")
        view?.send(sendIntent)

    }

    fun onCopyClicked() {
        Toast.makeText(
            view?.context(),
            view?.context()?.resources?.getString(R.string.invite_copied),
            Toast.LENGTH_LONG
        ).show()
        val clipboard = view?.context()?.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager?
        val clip = ClipData.newPlainText("code", code)
        clipboard?.primaryClip = clip
    }
}