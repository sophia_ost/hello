package com.s.hellositter.presentation.drawerparentmenu.settings.changepass

import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.databinding.ObservableField
import com.s.hellositter.R
import com.s.hellositter.domain.model.MainInteractor
import com.s.hellositter.presentation.base.BaseViewModel
import javax.inject.Inject

class ChangePassViewModel @Inject constructor(): BaseViewModel<ChangePassView>() {

    @Inject
    lateinit var mainInteractor: MainInteractor

    var oldPassBind: ObservableField<String> = ObservableField("")

    var newPassBind: ObservableField<String> = ObservableField("")

    var confirmPassBind: ObservableField<String> = ObservableField("")

    fun onSaveClicked(){
        mainInteractor.user.password?.apply {

                if (!oldPassBind.get().isNullOrEmpty() && mainInteractor.user.password == oldPassBind.get()){
                    if( !newPassBind.get().isNullOrEmpty() && !confirmPassBind.get().isNullOrEmpty() ){
                        if (newPassBind.get() == confirmPassBind.get()){
                            view?.returnToSettings()

                            // SEND REQUEST HERE
                        } else {
                            Toast.makeText(view?.context(), view?.context()?.resources?.getString(R.string.change_pass_not_match), Toast.LENGTH_SHORT).show()
                        }
                    }


                } else {
                    Toast.makeText(view?.context(), view?.context()?.resources?.getString(R.string.change_pass_wrong_old), Toast.LENGTH_SHORT).show()
                }

        }


    }

}