package com.s.hellositter.domain.model

data class Parent(
    var name: String? = null,
    var lastName: String? = null,
    var password: String? = null,
    var email: String? = null,
    var phoneNumber: String? = null,
    var homeStreet: String? = null,
    var homeApartment: String? = null,
    var homeCity: String? = null,
    var homeState: String? = null,
    var zipCode: Int? = 0,
    var kidsNumber: Int? = 0,
    var kidsInfo: List<Child>? = null,
    var pets: String? = null,
    var myPhoto: String? = null,
    var emergencyContacts: List<String>? = null,
    var workStreet: String? = null,
    var workApartment: String? = null,
    var payment: String? = null
)