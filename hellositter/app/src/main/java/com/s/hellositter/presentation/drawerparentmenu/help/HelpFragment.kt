package com.s.hellositter.presentation.drawerparentmenu.help

import android.content.Intent
import com.s.hellositter.R
import com.s.hellositter.presentation.base.BaseActivity
import com.s.hellositter.presentation.base.BaseFragment
import com.s.hellositter.presentation.navigate
import com.s.hellositter.presentation.permissionCheck
import com.s.hellositter.presentation.withActivity
import kotlinx.android.synthetic.main.activity_base.*

class HelpFragment: BaseFragment<HelpViewModel>(), HelpView {
    override fun getLayoutResource(): Int = R.layout.fragment_drawer_help

    override fun getViewModelClass(): Class<HelpViewModel> = HelpViewModel::class.java

    override fun viewInit() {
        withActivity<BaseActivity> {
            hideBottomNavigation()

        }
    }

    override fun navigateBack() {
        this@HelpFragment.navigate(R.id.action_help_fragment_to_book_tab)
    }

    override fun call(intent: Intent) {
        withActivity<BaseActivity> {
            permissionCheck(android.Manifest.permission.CALL_PHONE) {
                if (this) {
                    permissionCheck(android.Manifest.permission.CALL_PHONE){
                        if (this){
                            this@HelpFragment.startActivity(intent)
                        }
                    }

                }
            }
        }
    }


}